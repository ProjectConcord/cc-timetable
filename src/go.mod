module gitlab.com/ProjectConcord/cc-timetable/src/main

go 1.15

require (
	github.com/arangodb/go-driver v0.0.0-20201106193344-56ae8fd24510
	github.com/bitwurx/jrpc2 v0.0.0-20200508153510-d8310ad1baf0
)
